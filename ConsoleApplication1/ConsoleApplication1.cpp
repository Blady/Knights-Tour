// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stdio.h>
#include <conio.h>
#include <iostream>
using namespace std;

int** arr; // The array that holds all of our steps
int sizef = 8; // The size of the grid, default 8

// Make the method to calculate steps
int solveKTUtil(int x, int y, int movei, int** arr, int xMove[], int yMove[]);

// Check if the next step is viable (if the array is -1, and if it is within the borders of the grid)
bool isSafe(int x, int y, int** arr)
{
	return (x >= 0 && x < sizef && y >= 0 && y < sizef && arr[x][y] == -1);
}

// Get the array when path has been found and print each number in the path on a grid.
void printSolution(int** arr)
{
	for (int x = 0; x < sizef; x++)
	{
		for (int y = 0; y < sizef; y++)
		{
			printf(" %2d ", arr[x][y]);
		}
		printf("\n");
	}
}

// Method to solve the Knights Tour
bool solveKnightsTour()
{
	// Empty the array so that the anwsers can be filled in (default -1)
	for (int x = 0; x < sizef; x++)
	{
		for (int y = 0; y < sizef; y++)
		{
			arr[x][y] = -1;
		}
	}

	// Set the move values for the Knight (possible steps)
	int xMove[8] = { 2, 1, -1, -2, -2, -1,  1,  2 };
	int yMove[8] = { 1, 2,  2,  1, -1, -2, -2, -1 };

	// Start the Knight at point 0,0 (top left)
	arr[0][0] = 0;

	// Try each step to find a solution (prints line when there is no solution found)
	if (solveKTUtil(0, 0, 1, arr, xMove, yMove) == false)
	{
		printf("Solution does not exist");
		return false;
	}
	else
	{
		printSolution(arr);
	}
	return true;
}

// Go by each step to see if it's viable
int solveKTUtil(int x, int y, int movei, int** arr, int xMove[], int yMove[])
{
	// Values to hold the next step
	int k, next_x, next_y;

	// Check if each step has already been done. If so, the tour is completed
	if (movei == sizef*sizef)
	{
		return true;
	}

	// Try each possible step of the Knight to see if that space on the grid has not been visited yet AND is within borders. 
	// (With a seperated method called: isSafe)
	// It calls itself when a new palce has been found to try the next location.
	// If no new location has been found, it goes back 1 step and tries again from there.
	for (k = 0; k < 8; k++)
	{
		next_x = x + xMove[k];
		next_y = y + yMove[k];
		if (isSafe(next_x, next_y, arr))
		{
			arr[next_x][next_y] = movei;
			if (solveKTUtil(next_x, next_y, movei + 1, arr, xMove, yMove) == true)
			{
				return true;
			}
			else
			{
				arr[next_x][next_y] = -1;
			}
		}
	}
	return false;
}

int main()
{
	// Get user input to choose size of board
	int p;
	cout << "Please enter an integer value to determine the size of the board (default: 8*8): ";
	cin >> p;
	sizef = p;

	// Set array size from user input
	arr = new int*[sizef];
	for (int m = 0; m < sizef; m++)
	{
		arr[m] = new int[sizef];
	}

	// Solve the Knights Tour
	solveKnightsTour();

	// Press key to exit program
	printf("Press Any Key To Exit...");
	_getch();

	// Delete array in memory
	for (int m = 0; m < sizef; m++)
		delete[] arr[m];
	delete[] arr;
	return 0;
}
